
package com.tatahealth.EMR.Scripts.Consultation;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.DiagnosisPage;
import com.tatahealth.EMR.pages.Consultation.FollowUpPage;
import com.tatahealth.EMR.pages.Consultation.GeneralAdvicePage;
import com.tatahealth.EMR.pages.Consultation.GlobalPrintPage;
import com.tatahealth.EMR.pages.Consultation.PrescriptionPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class PrescriptionTest {
	
	@BeforeClass(alwaysRun=true,groups= {"Regression","Appointment_Doctor"})
	public static void beforeClass() throws Exception {
		ConsultationTest currentTest = new ConsultationTest();
		currentTest.loginAndStartConsultation("Prescription 1");
	}
	
	@AfterClass(groups= {"Regression","Appointment_Doctor"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Login_Doctor.driver");
	}
	public static Integer row = 0;
	public static String interactionMedicine = "CROCID ORAL SUSP";
	public static String duplicateMedicine = "";
	public static String templateName = "";
	public static List<String> medicine = new ArrayList<String>();
	public static String cimsMedicineName = "GENTARIL INJ 80mg";
	public static String updateTemplateMedicine = "";
	//public static String templateName = "";
	GlobalPrintPage gp = new GlobalPrintPage();
	public static String medicalKitName = "";
	SymptomTest smt = new SymptomTest();
	WebDriver driver;


	ExtentTest logger;
	
	public synchronized void addMedicine()throws Exception{
		
		logger = Reports.extent.createTest("EMR Add Medicine");
		PrescriptionPage prescription = new PrescriptionPage();
		
		Web_GeneralFunctions.click(prescription.getAddMedicineButton(Login_Doctor.driver, logger), "Click Add Medicine button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
	}
	
	public synchronized void saveMedicine()throws Exception{
		
		logger = Reports.extent.createTest("EMR Save Medicines  By Right Menu");
	//	FollowUpPage followUp = new FollowUpPage();
		PrescriptionPage prescription = new PrescriptionPage();
		
		Web_GeneralFunctions.click(prescription.saveMedicines(Login_Doctor.driver, logger), "Save medicines by right menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
	}
	
	public synchronized void clickAddTemplate()throws Exception{
		
		logger = Reports.extent.createTest("EMR click add as template button");
		PrescriptionPage prescription = new PrescriptionPage();
		
		Web_GeneralFunctions.click(prescription.getAddTemplateButton(Login_Doctor.driver, logger), "Get Add Template button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		
	}
	
	public synchronized void saveTemplate()throws Exception{
		
		logger = Reports.extent.createTest("EMR click save template button");
		PrescriptionPage prescription = new PrescriptionPage();
		
		Web_GeneralFunctions.click(prescription.getSaveTemplate(Login_Doctor.driver, logger), "Save medicine template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
	}
	
	public synchronized void selectTDHMedicine()throws Exception{
		
		logger = Reports.extent.createTest("EMR Check/Uncheck TDH check box ");
		PrescriptionPage prescription = new PrescriptionPage();
		System.out.println("select tdh catalog");
	//	moveToPrescriptionModule();
		
		Web_GeneralFunctions.click(prescription.getTDHCheckBox(Login_Doctor.driver, logger),"Check/Uncheck TDH medicine", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
	}
	
	public synchronized void selectCIMSMedicine()throws Exception{
		
		logger = Reports.extent.createTest("EMR Check/Uncheck CIMS check box ");
		PrescriptionPage prescription = new PrescriptionPage();
		System.out.println("select cims medicine");
		moveToPrescriptionModule();
		
		Web_GeneralFunctions.click(prescription.getCIMSCheckBox(Login_Doctor.driver, logger),"Check/Uncheck CIMS medicine", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
	}
	
	
	public synchronized void selectClinicSpecificMedicine()throws Exception{
		
		logger = Reports.extent.createTest("EMR Check/Uncheck clinic specific check box ");
		PrescriptionPage prescription = new PrescriptionPage();
		System.out.println("select clinic specific medicine");
	    moveToPrescriptionModule();
		
		Web_GeneralFunctions.click(prescription.getClinicSpecificCheckBox(Login_Doctor.driver, logger),"Check/Uncheck clinic specific medicine", Login_Doctor.driver, logger);
		Thread.sleep(2000);
	}
	
	public synchronized void clickCloseButtonInTemplate()throws Exception{
		
		logger = Reports.extent.createTest("EMR Close button in template");
		PrescriptionPage prescription = new PrescriptionPage();
		
		Web_GeneralFunctions.click(prescription.getCloseTemplateButton(Login_Doctor.driver, logger), "Get close button in template modal", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}

      //clicking the prescription button right side of the consultation page 
	@Test(groups= {"Regression","Login"},priority=724)
	public synchronized void moveToPrescriptionModule()throws Exception{
		
		logger = Reports.extent.createTest("EMR move to prescription module");
		DiagnosisPage diagnosisPrescription = new DiagnosisPage();
		
		
		Web_GeneralFunctions.click(diagnosisPrescription.moveToDiagnosisModule(Login_Doctor.driver, logger), "Click Diagnosis/prescription menu", Login_Doctor.driver, logger);
		Thread.sleep(100);
		
		System.out.println("moveToPrescriptionModule 1 done");
	}
	
	//Validating error message while adding new medicine row;
	@Test(groups= {"Regression","Login"},priority=725)
	public synchronized void emptyRowAlreadyExist()throws Exception{
		
		logger = Reports.extent.createTest("EMR empty row already exist");
		PrescriptionPage prescription = new PrescriptionPage();
		addMedicine(); // click on the add medicine button
		String text = Web_GeneralFunctions.getText(prescription.getMessage(Login_Doctor.driver, logger), "Get warning message", Login_Doctor.driver, logger);
		if(text.equalsIgnoreCase("Empty row already exists")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(500);
		System.out.println("emptyRowAlreadyExist validation done 2");
	}
	
	@Test(groups= {"Regression","Login"},priority=726)
	public synchronized void saveTemplateWithEmptyMedicine()throws Exception{
		
		logger = Reports.extent.createTest("EMR save template with empty medicines");
		PrescriptionPage prescription = new PrescriptionPage();
		clickAddTemplate();
		String freeText = RandomStringUtils.randomAlphabetic(7);
		
		Web_GeneralFunctions.sendkeys(prescription.getTemplateNameField(Login_Doctor.driver, logger), freeText, "Sending template name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		saveTemplate();
		String text = Web_GeneralFunctions.getText(prescription.getMessage(Login_Doctor.driver, logger), "Get error message", Login_Doctor.driver, logger);
		System.out.println("text : "+text);
		if(text.equalsIgnoreCase("Please enter at least one medicine")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Web_GeneralFunctions.wait(1);
		clickCloseButtonInTemplate();
		Web_GeneralFunctions.wait(5);
		System.out.println("saveTemplateWithEmptyMedicine is done -3");
	}
	
	@Test(groups= {"Regression","Login"},priority=727)
	public synchronized void selectMedicine()throws Exception{
		logger = Reports.extent.createTest("EMR select medicines");
		PrescriptionPage prescription = new PrescriptionPage();
		
		Web_GeneralFunctions.click(prescription.selectMedicineFromDropDown("", row, Login_Doctor.driver, logger), "Select random medicine from drop down", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		System.out.println("selectMedicine done 1");
	}
	
   @Test(groups= {"Regression","Login"},priority=728)
	public synchronized void saveMedicinesWithEmptyFrequency()throws Exception{
		
		logger = Reports.extent.createTest("EMR save medicines with empty frequency");
		PrescriptionPage prescription = new PrescriptionPage();
		saveMedicine();
		String text = Web_GeneralFunctions.getText(prescription.getMessage(Login_Doctor.driver, logger), "Get fill mandatory field message", Login_Doctor.driver, logger);
		if(text.equalsIgnoreCase("Please fill all mandatory field(s)")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Web_GeneralFunctions.wait(5);
		
		System.out.println("saveMedicinesWithEmptyFrequency is done 3");
	}
	
	@Test(groups= {"Regression","Login"},priority=729)
	public synchronized void enterFrequency()throws Exception{
		
		logger = Reports.extent.createTest("EMR enter frequency");
		PrescriptionPage prescription = new PrescriptionPage();
		String frequnecy = "1-1-1";
		
		Web_GeneralFunctions.clearWebElement(prescription.getFrequency(row, Login_Doctor.driver, logger), "clear frequency field", Login_Doctor.driver, logger);
		Thread.sleep(100);		
		
		Web_GeneralFunctions.sendkeys(prescription.getFrequency(row, Login_Doctor.driver, logger), frequnecy,"Sending frequnecy in medicine module", Login_Doctor.driver, logger);
		Thread.sleep(100);
		
		updateTemplateMedicine += frequnecy + " ";
		System.out.println("enterFrequency done 4");
	}
	
	 @Test(groups= {"Regression","Login"},priority=730)
	public synchronized void saveMedicinesWithEmptyDuration()throws Exception{
		
		logger = Reports.extent.createTest("EMR save medicines with empty duration");
		PrescriptionPage prescription = new PrescriptionPage();
		saveMedicine();
		Thread.sleep(100);

		String text = Web_GeneralFunctions.getText(prescription.getMessage(Login_Doctor.driver, logger), "Get fill mandatory field message", Login_Doctor.driver, logger);
		if(text.equalsIgnoreCase("Please fill all mandatory field(s)")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(100);
		System.out.println("saveMedicinesWithEmptyDuration done 5");
	}
	
	@Test(groups= {"Regression","Login"},priority=731)
	public synchronized void enterDuration()throws Exception{
		
		logger = Reports.extent.createTest("EMR enter duration");
		PrescriptionPage prescription = new PrescriptionPage();
		Random r = new Random();
		Integer duration = r.nextInt(20);
		if(duration == 0) {
			duration +=1;
		}
		Web_GeneralFunctions.clearWebElement(prescription.getDuration(row, Login_Doctor.driver, logger), "clear frequency field", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.sendkeys(prescription.getDuration(row, Login_Doctor.driver, logger), duration.toString(),"Sending frequnecy in medicine module", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		updateTemplateMedicine += duration.toString() + " ";
		System.out.println("enterDuration done 6");
	}
	
	@Test(groups= {"Regression","Login"},priority=732)
	public synchronized void saveMedicineWithEmptyMedicineName()throws Exception{
		
		row++;
		logger = Reports.extent.createTest("EMR save Medicine with empty medicine name");
		PrescriptionPage prescription = new PrescriptionPage();
		addMedicine();
		selectMedicine();
		Web_GeneralFunctions.clearWebElement(prescription.getDrugName(row, Login_Doctor.driver, logger), "Clear drug name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		saveMedicine();
		Web_GeneralFunctions.wait(1);
		String text = Web_GeneralFunctions.getText(prescription.getMessage(Login_Doctor.driver, logger), "Get fill mandatory field message", Login_Doctor.driver, logger);
		String text1=prescription.getMessage(Login_Doctor.driver, logger).getAttribute("innerHTML");
		System.out.println("text1"+text1);
		System.out.println("text :..... "+text+"Please fill all mandatory field");
		if(text.contains("Please fill all mandatory field")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(100);
		System.out.println("saveMedicineWithEmptyMedicineName done 7-failed");
	}
	
		@Test(groups= {"Regression","Login"},priority=733)
	public synchronized void chooseTDHMedicine()throws Exception{
		
		logger = Reports.extent.createTest("EMR choose tdh and search tdh medicine");
		PrescriptionPage prescription = new PrescriptionPage();
		DiagnosisPage dPage = new DiagnosisPage();
		//Web_GeneralFunctions.click(dPage.getDiagnosisTextField(1, Login_Doctor.driver, logger), "Click 1st row of diagnosis", Login_Doctor.driver, logger);
		//Thread.sleep(1000);
		selectCIMSMedicine();
		
		selectClinicSpecificMedicine();
		//selectTDHMedicine();

		//Thread.sleep(500);

		Web_GeneralFunctions.click(prescription.getDrugName(row, Login_Doctor.driver, logger), "Get drug name row", Login_Doctor.driver, logger);
		Thread.sleep(100);
		String text = Web_GeneralFunctions.getText(prescription.getMedicinMasterFromDropDown(row, "", Login_Doctor.driver, logger), "Get 1st medicine master", Login_Doctor.driver, logger);
		Thread.sleep(500);
		Web_GeneralFunctions.click(prescription.selectMedicineFromDropDown("tablet", row, Login_Doctor.driver, logger), "Get 1st medicine", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("text : "+text+"TDH Catalog");

		if(text.equalsIgnoreCase("TDH Catalog")) {
 			assertTrue(true, "List displaying TDH Catalog medecine");
		}else {
			assertTrue(false, "list not displaying TDH Catalog-displaying-"+text);
		}
		Thread.sleep(100);
		 System.out.println("chooseTDHMedicine done 8- failed");
	}
	
	@Test(groups= {"Regression","Login"},priority=734)
	public synchronized void enterDose()throws Exception{
		
		logger = Reports.extent.createTest("EMR enter dose");
		PrescriptionPage prescription = new PrescriptionPage();
		Random r = new Random();
		Integer dose = r.nextInt(10);
		if(dose == 0) {
			dose +=1;
		}
		
		Web_GeneralFunctions.clearWebElement(prescription.getDoseField(row, Login_Doctor.driver, logger), "clear frequency field", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.sendkeys(prescription.getDoseField(row, Login_Doctor.driver, logger), dose.toString(), "Enter dose", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		updateTemplateMedicine += dose.toString() +" ";
		
		System.out.println("enterDose done 9");
	}
	
 	@Test(groups= {"Regression","Login"},priority=735)
	public synchronized void saveMedicineWithEmptyDoseUnitField()throws Exception{
		
		logger = Reports.extent.createTest("EMR save medicine with empty dose unit");
		PrescriptionPage prescription = new PrescriptionPage();
		saveMedicine();
		String text = Web_GeneralFunctions.getText(prescription.getMessage(Login_Doctor.driver, logger), "Get fill mandatory field message", Login_Doctor.driver, logger);
		if(text.equalsIgnoreCase("Please fill all mandatory field(s)")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(1000);
		System.out.println("saveMedicineWithEmptyDoseUnitField done 10");
	}
	
	@Test(groups= {"Regression","Login"},priority=736)
	public synchronized void selectDoseUnit()throws Exception{
		
		logger = Reports.extent.createTest("EMR select dose unit");
		PrescriptionPage prescription = new PrescriptionPage();
		
		Web_GeneralFunctions.selectElement(prescription.getDoseUnitField(row, Login_Doctor.driver, logger), "mg", "select given dose unit", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		
		updateTemplateMedicine += "mg";
		System.out.println("selectDoseUnit done 11");
		
	}
	
	@Test(groups= {"Regression","Login"},priority=737)
	public synchronized void selectFrequnecyFromMasterList() throws Exception{
		
		logger = Reports.extent.createTest("EMR select frequency from master list");
		PrescriptionPage prescription = new PrescriptionPage();
		
		Web_GeneralFunctions.click(prescription.getSelectFrequencyFromDropDown("", row, Login_Doctor.driver, logger), "Select random frequnecy from drop down", Login_Doctor.driver, logger);
		Thread.sleep(1000);
System.out.println("selectFrequnecyFromMasterList done 12");
	}
	
	
	@Test(groups= {"Regression","Login"},priority=738)
	public synchronized void selectRegularlyDurationFromMasterList() throws Exception{
		
		logger = Reports.extent.createTest("EMR select Regularly duration from master list");
		PrescriptionPage prescription = new PrescriptionPage();
		
		Web_GeneralFunctions.click(prescription.getSelectRegularlyDurationFromDropDown("", row, Login_Doctor.driver, logger), "Select random frequnecy from drop down", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("selectRegularlyDurationFromMasterList done 13");
	}
	
	@Test(groups= {"Regression","Login"},priority=739)
	public synchronized void enterSplInstruction()throws Exception{
		
		logger = Reports.extent.createTest("EMR enter special instruction");
		PrescriptionPage prescription = new PrescriptionPage();
		String freeText = RandomStringUtils.randomAlphabetic(10);
		
		Web_GeneralFunctions.sendkeys(prescription.getFrequencyInstruction(row, Login_Doctor.driver, logger), freeText,"Enter special instruction", Login_Doctor.driver, logger);
		Thread.sleep(100);
		System.out.println("enterSplInstruction done 14");
		
	}
	
//........................................................	
	@Test(groups= {"Regression","Login"},priority=740) 
	public synchronized void chooseCIMSMedicine()throws Exception{ 
		  row++; 
		  logger = Reports.extent.createTest("EMR choose CIMS and search CIMS medicine");
	  PrescriptionPage prescription = new PrescriptionPage(); DiagnosisPage dPage =
	  new DiagnosisPage();
	  Web_GeneralFunctions.click(dPage.getDiagnosisTextField(1,Login_Doctor.driver, logger), "Click 1st row of diagnosis",Login_Doctor.driver, logger); Thread.sleep(1000); 
	  selectCIMSMedicine();
	  Thread.sleep(500);

	  selectTDHMedicine();
	  Web_GeneralFunctions.scrollElementByJavaScriptExecutor(prescription.getDrugName(0, Login_Doctor.driver, logger), "scroll to drug name 0",Login_Doctor.driver, logger); 
	  Thread.sleep(1000);
	  addMedicine();
	  
	  Web_GeneralFunctions.click(prescription.getDrugName(row, Login_Doctor.driver,logger), "Get drug name row", Login_Doctor.driver, logger);
	  Thread.sleep(1000); 
	  String text = Web_GeneralFunctions.getText(prescription.getMedicinMasterFromDropDown(row,"CARBADAC 200 TAB 200mg", Login_Doctor.driver, logger),
	  "Get 1st medicine master", Login_Doctor.driver, logger); Thread.sleep(100);
	  Web_GeneralFunctions.click(prescription.getFirstMedicineFromDropDown(row, "",Login_Doctor.driver, logger), "Get 1st medicine", Login_Doctor.driver,
	  logger); Thread.sleep(1000); System.out.println("text-"+text+"CIMS");
		/*
		 * if(text.equalsIgnoreCase("CIMS")) { assertTrue(true); } else {
		 * assertTrue(false); } Thread.sleep(1000);
		 */
	  System.out.println("chooseCIMSMedicine done 15-failed"); }
	 
	
	@Test(groups= {"Regression","Login"},priority=741)
	public synchronized void searchAndSelectFrequnecyFromMasterList() throws Exception{
		
		logger = Reports.extent.createTest("EMR search and select frequency from master list");
		PrescriptionPage prescription = new PrescriptionPage();
		
		Web_GeneralFunctions.click(prescription.getSelectFrequencyFromDropDown("1-1-0", row, Login_Doctor.driver, logger), "Select random frequnecy from drop down", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("searchAndSelectFrequnecyFromMasterList done 16");
	}
	
	@Test(groups= {"Regression","Login"},priority=742)
	public synchronized void durationInWeeks()throws Exception{
		
		logger = Reports.extent.createTest("EMR enter duration in weeks");
		PrescriptionPage prescription = new PrescriptionPage();
		enterDuration();
		Web_GeneralFunctions.selectElement(prescription.getDurationUnit(row, Login_Doctor.driver, logger), "WEEK", "Duration in weeks", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("durationInWeeks done 17");

	}
	
	// Failing because of manual issue
	 //@Test(groups= {"Regression","Login"},priority=750)
	public synchronized void drugToDrugInteraction()throws Exception{
	  
	  logger =
	  Reports.extent.createTest("EMR drug to drug interaction veriication pop up");
	  PrescriptionPage prescription = new PrescriptionPage(); 
	  addMedicine();
	  System.out.println("interaction medicine : "+interactionMedicine); 
	  row++;
	  
	  Web_GeneralFunctions.click(prescription.getFirstMedicineFromDropDown(row,
	  interactionMedicine, Login_Doctor.driver, logger), "Get 1st medicine",Login_Doctor.driver, logger); 
	  Thread.sleep(500); 
	  String text = Web_GeneralFunctions.getText(prescription.getInteractionDialogBox(
	  Login_Doctor.driver, logger), "Get negative interation dialog box",
	  Login_Doctor.driver, logger); Thread.sleep(500);
	  System.out.println("interaction text : "+text + "has negative interactions");
	  if(text.contains("has negative interactions")) { assertTrue(true); }else {
	  assertTrue(false); } Thread.sleep(100);
	  System.out.println("drugToDrugInteraction done 18-failed");
	  
	  }
	 
	
	
	// Failing because of manual issue
	//  @Test(groups = { "Regression", "Login" }, priority = 751) public synchronized
	  void clickNOdrugToDrugInteractionPopUp() throws Exception {
	  
	  logger = Reports.extent.
	  createTest("EMR click NO button in drug to drug interaction veriication pop up"
	  ); PrescriptionPage prescription = new PrescriptionPage();
	  
	  Web_GeneralFunctions.click(prescription.getNOInInteractionPopUp(Login_Doctor. driver, logger), "click No button in interaction pop up",
	  Login_Doctor.driver, logger); Thread.sleep(100);
	  Web_GeneralFunctions.click(prescription.getDeleteRow(row, Login_Doctor.driver, logger), "delete row", Login_Doctor.driver, logger);
	  Thread.sleep(1000); saveMedicine();
	  moveToPrescriptionModule(); 
	  String text =Web_GeneralFunctions.getAttribute(prescription.getDrugName(row,Login_Doctor.driver, logger), "value", "Get drug name value",
	  Login_Doctor.driver, logger); Thread.sleep(1000); if (text.isEmpty()) {
	  assertTrue(true); } else { assertTrue(false); } Thread.sleep(100); }
	 
	 
		// Failing because of manual issue

	//  @Test(groups= {"Regression","Login"},priority=760) 
	  public synchronized void clickYesdrugToDrugInteractionPopUp()throws Exception{
	  
	  row++; //drugToDrugInteraction(); //selectTDHMedicine();
	  selectCIMSMedicine(); addMedicine(); 
	  logger = Reports.extent.createTest("EMR click yes button in drug to drug interaction veriication pop up"
	  ); PrescriptionPage prescription = new PrescriptionPage();
	  System.out.println("interaction medicine in yes : "+interactionMedicine);
	  Web_GeneralFunctions.click(prescription.getFirstMedicineFromDropDown(row,interactionMedicine, Login_Doctor.driver, logger), "Get 1st medicine",
	  Login_Doctor.driver, logger); Thread.sleep(3000);
	  Web_GeneralFunctions.click(prescription.getInteractionDialogBox(Login_Doctor.driver, logger), "Get Interaction dialog box", Login_Doctor.driver, logger);
	  Thread.sleep(1000);
	  Web_GeneralFunctions.click(prescription.getYesInInteractionPopUp(Login_Doctor.driver, logger), "click Yes button in interaction pop up",
	  Login_Doctor.driver, logger); Thread.sleep(2000); enterFrequency();
	  enterDuration(); //generalFunctions.click(prescription.getDeleteRow(row,Login_Doctor.driver, logger),"delete row", Login_Doctor.driver, logger);
	  //Thread.sleep(1000); 
	  saveMedicine(); 
	  moveToPrescriptionModule(); 
	  String text= Web_GeneralFunctions.getAttribute(prescription.getDrugName(row,Login_Doctor.driver, logger), "value", "Get drug name value",Login_Doctor.driver, logger); Thread.sleep(1000);
	  if(text.equalsIgnoreCase(interactionMedicine)) { assertTrue(true); }else {
	  assertTrue(false); } }
	 
	
	@Test(groups= {"Regression","Login"},priority=743)
	public synchronized void unselectAllMaster()throws Exception{
		
		logger = Reports.extent.createTest("EMR unselect all masters");
		PrescriptionPage prescription = new PrescriptionPage();
		selectCIMSMedicine();
		String text = Web_GeneralFunctions.getText(prescription.getMessage(Login_Doctor.driver, logger), "Get Master removal error message", Login_Doctor.driver, logger);
		Thread.sleep(100);
		System.out.println("text"+text+"At least one drug database should be selected");
		if(text.equalsIgnoreCase("At least one drug database should be selected")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Web_GeneralFunctions.wait(1);
		System.out.println("unselectAllMaster done 20 - failed");

	}
	
	
	@Test(groups= {"Regression","Login"},priority=744)
	public synchronized void chooseClinicSpecificMedicine()throws Exception{
	      row++;
		logger = Reports.extent.createTest("EMR choose clinic specific medicine master and search clinic specific medicine");
		PrescriptionPage prescription = new PrescriptionPage();
		DiagnosisPage dPage = new DiagnosisPage();
		Web_GeneralFunctions.click(dPage.getDiagnosisTextField(1, Login_Doctor.driver, logger), "Click 1st row of diagnosis", Login_Doctor.driver, logger);
		Thread.sleep(100);
		//selectTDHMedicine();
		selectCIMSMedicine();
		selectClinicSpecificMedicine();
		Web_GeneralFunctions.scrollElementByJavaScriptExecutor(prescription.getDrugName(0, Login_Doctor.driver, logger), "scroll to drug name 0",Login_Doctor.driver, logger); 
		Thread.sleep(1000);
		addMedicine();
		
		Web_GeneralFunctions.click(prescription.getDrugName(row, Login_Doctor.driver, logger), "Get drug name row", Login_Doctor.driver, logger);
		Thread.sleep(500);
		String text = Web_GeneralFunctions.getText(prescription.getMedicinMasterFromDropDown(row, "Froben", Login_Doctor.driver, logger), "Get 1st medicine master", Login_Doctor.driver, logger);
		Thread.sleep(500);
		Web_GeneralFunctions.click(prescription.getFirstMedicineFromDropDown(row, "", Login_Doctor.driver, logger), "Get 1st medicine", Login_Doctor.driver, logger);
		Thread.sleep(100);
		System.out.println("text"+text +"Clinic Master");
		if(text.equalsIgnoreCase("Clinic Master")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(500);
		System.out.println("chooseClinicSpecificMedicine done 21-failed");

		 
	}
	
	@Test(groups= {"Regression","Login"},priority=745)
	public synchronized void changeAdvice()throws Exception{
		
		logger = Reports.extent.createTest("EMR change dosage advice");
		PrescriptionPage prescription = new PrescriptionPage();
		enterFrequency();
		enterDuration();
		Web_GeneralFunctions.selectElement(prescription.getDosageAdvice(row, Login_Doctor.driver, logger),"bf","Change advice field as Before Food", Login_Doctor.driver, logger);
		Thread.sleep(500);
		System.out.println("changeAdvice done 22");
		
	}
	
	@Test(groups= {"Regression","Login"},priority=746)
	public synchronized void selectFutureDate()throws Exception{
		
		logger = Reports.extent.createTest("EMR select future date in start date field");
		PrescriptionPage prescription = new PrescriptionPage();
		
		Web_GeneralFunctions.click(prescription.getStartDate(row, Login_Doctor.driver, logger),"Click start date field", Login_Doctor.driver, logger);
		Thread.sleep(500);
		Web_GeneralFunctions.click(prescription.getFutureDate(Login_Doctor.driver, logger), "select Future date from date picker", Login_Doctor.driver, logger);
		Thread.sleep(500);
		System.out.println("selectFutureDate done 23");

		
	}
	
	@Test(groups= {"Regression","Login"},priority=747)
	public synchronized void saveSuccessfully()throws Exception{
		
		logger = Reports.extent.createTest("EMR save the changes successfully");
		PrescriptionPage prescription = new PrescriptionPage();
		saveMedicine();
		Web_GeneralFunctions.wait(2);
		String text = Web_GeneralFunctions.getText(prescription.getMessage(Login_Doctor.driver, logger), "Get success message", Login_Doctor.driver, logger);
		if(text.equalsIgnoreCase("Diagnosis and Prescription details saved successfully")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		System.out.println("saveSuccessfully done 24");

	}
	
	@Test(groups= {"Regression","Login"},priority=748)
	public synchronized void duplicateMedicine()throws Exception{
		
		logger = Reports.extent.createTest("EMR duplicate medicines");
		PrescriptionPage prescription = new PrescriptionPage();
	//	saveMedicine();
		row++;
		moveToPrescriptionModule();
		duplicateMedicine = Web_GeneralFunctions.getAttribute(prescription.getDrugName(0, Login_Doctor.driver, logger), "value", "Get 1st prescribed drug name", Login_Doctor.driver, logger);
		//System.out.println("text duplicate text : "+duplicateMedicine);
		
		Web_GeneralFunctions.sendkeys(prescription.getDrugName(row, Login_Doctor.driver, logger), duplicateMedicine, "Sending duplicate values in drug name field", Login_Doctor.driver, logger);
		Thread.sleep(100);
		String text = Web_GeneralFunctions.getText(prescription.getDuplicateBox(Login_Doctor.driver, logger), "Get duplicate box", Login_Doctor.driver, logger);
		//System.out.println("duplicate medicine : "+text);
		Thread.sleep(100);
		if(text.contains("Duplicate Medicine Found")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		System.out.println("saveSuccessfully done 25");

	}

	@Test(groups= {"Regression","Login"},priority=749)
	public synchronized void clickNODuplicateBoxPopUp()throws Exception{
		
		logger = Reports.extent.createTest("EMR click NO button in duplicate medicine pop up");
		PrescriptionPage prescription = new PrescriptionPage();
		
		Web_GeneralFunctions.click(prescription.getNOButtonInDuplicateBox(Login_Doctor.driver, logger), "click No button in duplicate medicine pop up", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(prescription.getDeleteRow(row, Login_Doctor.driver, logger),"delete row", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		saveMedicine();
		moveToPrescriptionModule();
		String text = Web_GeneralFunctions.getAttribute(prescription.getDrugName(row, Login_Doctor.driver, logger), "value", "Get drug name value", Login_Doctor.driver, logger);
		Thread.sleep(100);
		if(text.isEmpty()) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		System.out.println("clickNODuplicateBoxPopUp done 26");

	}
	
	@Test(groups= {"Regression","Login"},priority=750)
	public synchronized void clickYesDuplicateMedicinePopUp()throws Exception{
		
		//row--;
	//	duplicateMedicine();
		logger = Reports.extent.createTest("EMR click yes button in duplicate medicine pop up");
		PrescriptionPage prescription = new PrescriptionPage();
		Web_GeneralFunctions.sendkeys(prescription.getDrugName(row, Login_Doctor.driver, logger), duplicateMedicine, "Sending duplicate values in drug name field", Login_Doctor.driver, logger);
		Thread.sleep(2000);
		/*
		 * Web_GeneralFunctions.click(prescription.getDuplicateBox(Login_Doctor.driver,
		 * logger), "Get duplicate medicine dialog box", Login_Doctor.driver, logger);
		 * Thread.sleep(1000);
		 */
		String yes = Web_GeneralFunctions.getText(prescription.getYesButtonInDuplicateBox(Login_Doctor.driver, logger), "get yes value", Login_Doctor.driver, logger);
		Web_GeneralFunctions.doubleClick(prescription.getYesButtonInDuplicateBox(Login_Doctor.driver, logger), "click Yes button in duplicate medicine pop up", Login_Doctor.driver, logger);
		//Web_GeneralFunctions.click(prescription.getYesButtonInDuplicateBox(Login_Doctor.driver, logger), "click Yes button in duplicate medicine pop up", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		
		System.out.println("yes : "+yes);
		enterFrequency();
		enterDuration();
		//generalFunctions.click(prescription.getDeleteRow(row, Login_Doctor.driver, logger),"delete row", Login_Doctor.driver, logger);
		//Thread.sleep(1000);
		saveMedicine();
		moveToPrescriptionModule();
		String text = Web_GeneralFunctions.getAttribute(prescription.getDrugName(row, Login_Doctor.driver, logger), "value", "Get drug name value", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		if(text.equalsIgnoreCase(duplicateMedicine)) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
	}
	
	
	
	@Test(groups= {"Regression","Login"},priority=751)
	public synchronized void freeTextInMedicine()throws Exception{
		
		logger = Reports.extent.createTest("EMR free text in medicine name field");
		PrescriptionPage prescription = new PrescriptionPage();
		String freeText = RandomStringUtils.randomAlphabetic(10);
		System.out.println("row : "+row);
		
		Web_GeneralFunctions.sendkeys(prescription.getDrugName(row, Login_Doctor.driver, logger), freeText, "Sending free text value", Login_Doctor.driver, logger);
		Thread.sleep(100);
		freeText = RandomStringUtils.randomAlphanumeric(6);
		Web_GeneralFunctions.sendkeys(prescription.getFrequency(row, Login_Doctor.driver, logger), freeText, "Sending free text value in frequency", Login_Doctor.driver, logger);
		Thread.sleep(100);
		enterDuration();
		Thread.sleep(500);
		System.out.println("freeTextInMedicine done 27");

	}
	
	@Test(groups= {"Regression","Login"},priority=752)
	public synchronized void saveTemplateWithSuccess()throws Exception{
		
		logger = Reports.extent.createTest("EMR save template");
		PrescriptionPage prescription = new PrescriptionPage();
		clickAddTemplate();
		templateName  = RandomStringUtils.randomAlphabetic(7);
		Web_GeneralFunctions.clearWebElement(prescription.getTemplateNameField(Login_Doctor.driver, logger), "Clear template name", Login_Doctor.driver, logger);
		Thread.sleep(500);
		
		Web_GeneralFunctions.sendkeys(prescription.getTemplateNameField(Login_Doctor.driver, logger), templateName, "Sending template name in save template field", Login_Doctor.driver, logger);
		Thread.sleep(100);
		saveTemplate();
		String text = Web_GeneralFunctions.getText(prescription.getMessage(Login_Doctor.driver, logger), "Get success message", Login_Doctor.driver, logger);
		Thread.sleep(500);
		System.out.println("tesmplate text : "+text);
		if(text.equalsIgnoreCase("Prescription Template Saved Successfully")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(100);
		
		System.out.println("saveTemplateWithSuccess done 28");

		
	}
	
	@Test(groups= {"Regression","Login"},priority=753)
	public synchronized void duplicateTemplateName()throws Exception{
		
		logger = Reports.extent.createTest("EMR save template with duplicate template name");
		PrescriptionPage prescription = new PrescriptionPage();
		clickAddTemplate();
		Thread.sleep(100);

		Web_GeneralFunctions.sendkeys(prescription.getTemplateNameField(Login_Doctor.driver, logger), templateName, "Sending duplicate name in the save template pop up", Login_Doctor.driver, logger);
		Thread.sleep(100);
		saveTemplate();
		String text = Web_GeneralFunctions.getText(prescription.getMessage(Login_Doctor.driver, logger), "Get duplicate error message", Login_Doctor.driver, logger);
		Thread.sleep(100);
		System.out.println("duplicate text : "+text);
		if(text.equalsIgnoreCase("This Prescription Template name already exists. Please try another name")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(100);
		clickCloseButtonInTemplate();
		Thread.sleep(1000);
		System.out.println("duplicateTemplateName done 29");

	}
	
	//@Test(groups= {"Regression","Login"},priority=764)
	public synchronized void selectTemplate()throws Exception{
		
		logger = Reports.extent.createTest("EMR select template name");
		PrescriptionPage prescription = new PrescriptionPage();
		
		Web_GeneralFunctions.click(prescription.getTemplateFromDropDown(templateName, Login_Doctor.driver, logger), "Get template from drop down", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		//duplicateCheck();
		saveMedicine();
		String text = Web_GeneralFunctions.getText(prescription.getMessage(Login_Doctor.driver, logger), "Get success message", Login_Doctor.driver, logger);
		Thread.sleep(100);
		if(text.equalsIgnoreCase("Diagnosis and Prescription details saved successfully")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(100);
		moveToPrescriptionModule();
		Thread.sleep(100);
		System.out.println("duplicateTemplateName done 30");

	}
	
//	@Test(groups= {"Regression","Login"},priority=765)
	public synchronized void duplicateCheck()throws Exception{
		

		logger = Reports.extent.createTest("EMR duplicate check");
		PrescriptionPage prescription = new PrescriptionPage();
		Thread.sleep(1000);
		String text = Web_GeneralFunctions.getText(prescription.getDuplicateBox(Login_Doctor.driver, logger), "Get duplicate box", Login_Doctor.driver, logger);
		System.out.println("duplicate medicine : "+text);
		boolean status = prescription.duplicateBox(Login_Doctor.driver, logger);
		System.out.println("status :"+status);
		
		if(status == true) {
			text = Web_GeneralFunctions.getText(prescription.getDuplicateBox(Login_Doctor.driver, logger), "Get duplicate box", Login_Doctor.driver, logger);
			System.out.println("duplicate medicine : "+text);
			Thread.sleep(1000);
			
			Web_GeneralFunctions.click(prescription.getNOButtonInDuplicateBox(Login_Doctor.driver, logger), "click No button in duplicate medicine pop up", Login_Doctor.driver, logger);
			Thread.sleep(500);
			duplicateCheck();
		}else {
			System.out.println("not displaying");
		}
		System.out.println("duplicateCheck done 31");

	}
	
	
	
//	@Test(groups= {"Regression","Login"},priority=766)
	public synchronized void savedTemplatePresentInTemplateField()throws Exception{
		
		//getTemplateName
		logger = Reports.extent.createTest("EMR saved template present in template drop down");
		PrescriptionPage prescription = new PrescriptionPage();
		
		String text = Web_GeneralFunctions.getText(prescription.getTemplateName(templateName, Login_Doctor.driver, logger), "Get searched template name", Login_Doctor.driver, logger);
		Thread.sleep(100);
		System.out.println("templateName is "+ templateName);
		if(text.equalsIgnoreCase(templateName)) {
			assertTrue(true);
		}else{
			assertTrue(false);
		}
		Thread.sleep(100);
		
		medicine = prescription.getPrescriptionData(0, Login_Doctor.driver, logger);
		System.out.println("medicine list : "+medicine);
		Thread.sleep(100);
		System.out.println("savedTemplatePresentInTemplateField done 32");

	}
	
	
	/*
	 * @Test(groups= {"Regression","Login"},priority=936) public synchronized void
	 * clickMedicalKit()throws Exception{
	 * 
	 * logger = Reports.extent.createTest("EMR save as medical kit");
	 * Web_GeneralFunctions.click(gp.getSaveAsMedicalKit(Login_Doctor.driver,
	 * logger), "Click save as medical kit", Login_Doctor.driver, logger);
	 * Thread.sleep(1000); }
	 * 
	 * @Test(groups= {"Regression","Login"},priority=937) public synchronized void
	 * saveMedicalKitWithoutName()throws Exception{
	 * 
	 * logger = Reports.extent.createTest("EMR save  medical kit without kit name");
	 * GlobalPrintTest ptest = new GlobalPrintTest();
	 * 
	 * ptest.saveMedicalKit(); String message =
	 * Web_GeneralFunctions.getText(gp.getMessage(Login_Doctor.driver, logger),
	 * "Get warning message", Login_Doctor.driver, logger); Thread.sleep(1000);
	 * System.out.println("message : "+message);
	 * if(message.equalsIgnoreCase("Please enter medical kit name")) {
	 * assertTrue(true); }else { assertTrue(false); } Thread.sleep(1000); }
	 * 
	 * @Test(groups= {"Regression","Login"},priority=938) public synchronized void
	 * saveMedicalKitWithProperValue()throws Exception{ GlobalPrintTest ptest = new
	 * GlobalPrintTest();
	 * 
	 * logger = Reports.extent.createTest("EMR  medical kit with proper values"); //
	 * smt.Consultation_001(); //smt.Consultation_002();
	 * 
	 * medicalKitName = RandomStringUtils.randomAlphabetic(10);
	 * Web_GeneralFunctions.sendkeys(gp.getMedicalKitName(Login_Doctor.driver,
	 * logger), medicalKitName, "Sending medical kit value", Login_Doctor.driver,
	 * logger); Thread.sleep(1000); ptest.saveMedicalKit(); String message =
	 * Web_GeneralFunctions.getText(gp.getMessage(Login_Doctor.driver, logger),
	 * "Get success message", Login_Doctor.driver, logger); Thread.sleep(1000);
	 * if(message.equalsIgnoreCase("Medical kit created successfully.")) {
	 * assertTrue(true); }else { assertTrue(false); } Thread.sleep(1000); }
	 * 
	 * @Test(groups= {"Regression","Login"},priority=939) public synchronized void
	 * saveMedicalKitWithDuplicateName()throws Exception{ GlobalPrintTest ptest =
	 * new GlobalPrintTest();
	 * 
	 * logger =
	 * Reports.extent.createTest("EMR  medical kit with duplicate kit name");
	 * clickMedicalKit();
	 * 
	 * Web_GeneralFunctions.sendkeys(gp.getMedicalKitName(Login_Doctor.driver,
	 * logger), medicalKitName, "sending duplicate value", Login_Doctor.driver,
	 * logger); Thread.sleep(1000); ptest.saveMedicalKit(); String text =
	 * Web_GeneralFunctions.getText(gp.getMessage(Login_Doctor.driver, logger),
	 * "Get duplicate alert", Login_Doctor.driver, logger); Thread.sleep(1000);
	 * if(text.equalsIgnoreCase("Medical kit name already exists.")) {
	 * assertTrue(true); }else { assertTrue(false); } Thread.sleep(1000);
	 * ptest.closeMedicalKitButton(); Thread.sleep(1000); }
	 * 
	 * @Test(groups= {"Regression","Login"},priority=940) public synchronized void
	 * savedMedicalKitPresentInMedicalKitModule()throws Exception{
	 * 
	 * logger = Reports.extent.
	 * createTest("EMR saved medical kit present in medical kit module");
	 * Web_GeneralFunctions.click(gp.moveToSymptomModule(Login_Doctor.driver,
	 * logger), "move to symptom module", Login_Doctor.driver, logger);
	 * Thread.sleep(1000); String text =
	 * Web_GeneralFunctions.getText(gp.getMedicalKitFromDropDown(medicalKitName,
	 * Login_Doctor.driver, logger), "Get medical kit name", Login_Doctor.driver,
	 * logger); Thread.sleep(1000); if(text.equalsIgnoreCase(medicalKitName)) {
	 * assertTrue(true); }else { assertTrue(false); } Thread.sleep(1000);
	 * 
	 * }
	 */
	
	
	//@Test(groups= {"Regression","Login"},priority=768)
	public synchronized void printPDF()throws Exception{
		boolean prescriptionStatus = false;
		logger = Reports.extent.createTest("EMR Prescription Print  PDF");
		PrescriptionPage prescription = new PrescriptionPage();
		//medicine = prescription.getPrescriptionData(row, Login_Doctor.driver, logger);
	//	savedDiagnosis = diagnosis.getDiagnosisData(row, Login_Doctor.driver, logger);
		GlobalPrintTest gpt = new GlobalPrintTest();
		gpt.moveToPrintModule();
		gpt.print();
		String pdfContent = gpt.getPDFPage();
		System.out.println("pdf content ..: "+pdfContent);
		
		if(pdfContent.contains("Prescription")) {
			
			int i =0;
			while(i<medicine.size()) {
				System.out.println("medicine print: "+medicine.get(i));
				if(pdfContent.contains(medicine.get(i))) {
					prescriptionStatus = true;
				}else {
					prescriptionStatus = false;
					break;
				}
				i++;
			}
			
			
		}else {
			prescriptionStatus = false;
		}
		
		if(prescriptionStatus == true) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(2000);
	}
	
	
	@Test(groups= {"Regression","Diagnosis"},priority=754)
	   public synchronized void checkOutWithOutDiagonosis()throws Exception{
		DiagnosisTest2 p2 =new DiagnosisTest2();
			//moveToPrescriptionModule();
 			p2.Clickcheckout();
			
			//div[@class='sweet-alert showSweetAlert visible']
		}
		
		@Test(groups= {"Regression","Diagnosis"},priority=755)

	   public synchronized void ValidateWorningPopUpNoForCheckOut()throws Exception{
			DiagnosisTest2 p2 =new DiagnosisTest2();

			 p2.worningPopUpIsPresent();
			 System.out.println("...");

			p2. clickOnWorningPopUpNo();
			 System.out.println("...");
	     	Thread.sleep(200);
		 
	 

	}
		   @Test(groups= {"Regression","Diagnosis"},priority=756)

		   public synchronized void ValidateWorningPopUpYesForCheckOut()throws Exception
		   {
				DiagnosisTest2 p2 =new DiagnosisTest2();
	
			p2.Clickcheckout();
			p2.clickOnWorningPopUpYes();
			Thread.sleep(2000); 
		}
	
		   
		   @Test(groups= {"Regression","Diagnosis"},priority=757)
			  
			  public synchronized void ValidatesecondWorningPopUp()throws Exception{
				DiagnosisTest2 p2 =new DiagnosisTest2();

			  String WorningpopUp=
			  "//div[@id='checkoutConsultSummary']//div[@class='modal-header']"; String
			  WorningpopUpClose= "//a[@id='checkoutClose']"; String
			  WorningpopUpPrintSummarybtn= "//a[@id='printSummarybtn']"; try {
			  //driver.findElement(By.xpath(WorningpopUp)).isDisplayed();
			//  assertTrue(driver.findElement(By.xpath(WorningpopUp)).isDisplayed(), "WorningpopUp displayed");
			  driver.findElement(By.xpath(WorningpopUpClose)).click(); Thread.sleep(200);
				p2.Clickcheckout();
			  //driver.findElement(By.xpath(WorningpopUpPrintSummarybtn)).click();
			  //Thread.sleep(200);
			  
			  } catch (Exception e) { // TODO: handle exception 
				  } }
	//@Test(groups= {"Regression","Login"},priority=769)
	public synchronized void repeatPrescription()throws Exception{
		
		logger = Reports.extent.createTest("EMR repeat prescription");
		PrescriptionPage prescription = new PrescriptionPage();
		//GeneralAdvicePage gap = new GeneralAdvicePage();
		Web_GeneralFunctions.click(prescription.getDrugName(0, Login_Doctor.driver, logger), "Click 1st row in drug name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		Web_GeneralFunctions.click(prescription.getRepeatPrescription(Login_Doctor.driver, logger), "click repeat prescription", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		String text = Web_GeneralFunctions.getText(prescription.getRepeatPrescriptionSweetAlertMessage(Login_Doctor.driver, logger), "Get alert message", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		if(text.equalsIgnoreCase("Do you want to add drugs from old prescription?")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		
		Web_GeneralFunctions.wait(1);
		
	}
	
	//@Test(groups= {"Regression","Login"},priority=770)
	public synchronized void repeatPrescriptionNo()throws Exception{
		
		logger = Reports.extent.createTest("EMR repeat prescription no");
		PrescriptionPage prescription = new PrescriptionPage();
		Web_GeneralFunctions.click(prescription.getRepeatPrescriptionNo(Login_Doctor.driver, logger), "click no message", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
	}
	
	//@Test(groups= {"Regression","Login"},priority=771)
	public synchronized void repeatPrescriptionYes()throws Exception{
		
		logger = Reports.extent.createTest("EMR repeat prescription yes");
		PrescriptionPage prescription = new PrescriptionPage();
		Web_GeneralFunctions.click(prescription.getRepeatPrescription(Login_Doctor.driver, logger), "click repeat prescription", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(prescription.getRepeatPrescriptionYes(Login_Doctor.driver, logger), "click yes message", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		String yes = Web_GeneralFunctions.getText(prescription.getMessage(Login_Doctor.driver, logger), "get success message", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		if(yes.equalsIgnoreCase("Drugs from the previous prescription added successfully")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
	}
	
	
}

