package com.tatahealth.EMR.Scripts.PatientAdvanceSearch;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;

import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.AllSlotsPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.AppointmentsPage;
import com.tatahealth.EMR.pages.CommonPages.CommonPage;
import com.tatahealth.EMR.pages.PatientAdvanceSearch.AdvancePatientSearch;
import com.tatahealth.EMR.pages.PatientAdvanceSearch.PatientSearch;
import com.tatahealth.EMR.pages.RegisterPatient.PatientRegisterPage;
import com.tatahealth.EMR.pages.RegisterPatient.PatientRegisterPopup;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.Web_Testbase;

public class InLineSearch {

	public static ExtentTest logger;
	AdvancePatientSearch adPatSearch = new AdvancePatientSearch();
	AppointmentsPage appoiPage = new AppointmentsPage();
	AllSlotsPage asPage = new AllSlotsPage();
	PatientSearch patSearch=new PatientSearch();
	
	PatientRegisterPage parPage = new PatientRegisterPage();
	PatientRegisterPopup popupPage = new PatientRegisterPopup();
	CommonPage cPage=new CommonPage();

	private static String specificRole;
	public static final String BOOK_APPOINTMENT="Book Appointment ";
	@BeforeClass(groups= {"Regression","PatientAdvanceSearch"})
	@Parameters("role")
	public static void beforeClass(String role) throws Exception{
		Login_Doctor.executionName="Patient Inline Search";
		Login_Doctor.LoginTestwithDiffrentUser(role);
		specificRole=role;
	}

	@AfterClass(groups= {"Regression","PatientAdvanceSearch"})
	public static void afterClass(){
		Login_Doctor.LogoutTest();
		Reports.extent.flush();
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch76() throws Exception {
		logger=Reports.extent.createTest("Verify Inline Advance Search Option");
		
		if(specificRole.equals("NewCalCentre")||specificRole.equals("NEWDOCNOCALCENTRE")) {
		//Moving to All slots Page
		Web_GeneralFunctions.click(appoiPage.getAllSlotsPage(Login_Doctor.driver, logger), "Moving to All slots page from Appointments Page", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);
		}
		//Getting Available Slot Code
		int n = asPage.getAvailableSlot(Login_Doctor.driver, logger);
		String slotId = asPage.getSlotId(n, Login_Doctor.driver, logger);
		String prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
				
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.scrollElementToCenter(asPage.getPlusIconforAvailableSlot(slotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(asPage.getPlusIconforAvailableSlot(slotId, Login_Doctor.driver, logger), "Clicking on Plus Icon", Login_Doctor.driver, logger);
		
		//UHID search
		String validInput="*"+SheetsAPI.getDataProperties(Web_Testbase.input+".UHID");
		Web_GeneralFunctions.sendkeys(asPage.getSearchBoxinAvailableSlot(slotId, Login_Doctor.driver, logger), validInput, "Searching based on UHID", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(adPatSearch.getUHIDDetail(Login_Doctor.driver, logger).getText().contains(validInput.substring(1)),"Verifying if the result has same UHID like the entered value");
		asPage.getSearchBoxinAvailableSlot(slotId, Login_Doctor.driver, logger).clear();
		
		//Patient Name Search
		validInput=SheetsAPI.getDataProperties(Web_Testbase.input+".PatientName");
		Web_GeneralFunctions.sendkeys(asPage.getSearchBoxinAvailableSlot(slotId, Login_Doctor.driver, logger), validInput, "Searching based on Name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		List<WebElement> resultNames = adPatSearch.getPatientNameSearchResultSelect(Login_Doctor.driver, logger);
		for (WebElement name : resultNames) {
			Assert.assertTrue(name.getText().toLowerCase().contains(validInput.toLowerCase()),"List of Element Searched with Valid Name");
		}
		asPage.getSearchBoxinAvailableSlot(slotId, Login_Doctor.driver, logger).clear();
		
		//Mobile no
		validInput=SheetsAPI.getDataProperties(Web_Testbase.input+".MobileNumber");
		Web_GeneralFunctions.sendkeys(asPage.getSearchBoxinAvailableSlot(slotId, Login_Doctor.driver, logger), validInput, "Searching based on Mobile number", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		boolean res;
			List<WebElement> results=patSearch.getInlineResultContainer(Login_Doctor.driver, logger);
			for(int i=0;i<results.size();i++)
			{

				List<WebElement>phonenum=patSearch.getMobileNoFromInlineSearchResult(i+1,Login_Doctor.driver, logger);
				if(phonenum==null)
				{
					Assert.fail("No results are found");
				}
				else if(phonenum.size()==1)
					Assert.assertTrue(phonenum.get(0).getText().contains(validInput));
				else
				{
				res=phonenum.get(0).getText().contains(validInput)||phonenum.get(1).getText().contains(validInput);
				Assert.assertTrue(res);
				}
			}
		asPage.getSearchBoxinAvailableSlot(slotId, Login_Doctor.driver, logger).clear();
			
			
		//Clinic ID
		validInput="#"+SheetsAPI.getDataProperties(Web_Testbase.input+".ClinicID");
		Web_GeneralFunctions.sendkeys(asPage.getSearchBoxinAvailableSlot(slotId, Login_Doctor.driver, logger), validInput, "Searching based on Clinic ID", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		String patientName=adPatSearch.getPatientNameSearchResultSelect(Login_Doctor.driver, logger).get(0).getText();
		bookAppointment();
		
		Web_GeneralFunctions.scrollElementByJavaScriptExecutor(asPage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), "Scroll to the element",Login_Doctor.driver,logger);
		Web_GeneralFunctions.wait(5);
		Web_GeneralFunctions.click(adPatSearch.getAppointmentPhoto(Login_Doctor.driver, logger,patientName),"Click on Profile Pic", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);
		String patientClinic=adPatSearch.getClinicIDInPatientDetails(Login_Doctor.driver,logger).getAttribute("value");
		Assert.assertTrue(patientClinic.equals(validInput.substring(1)), "Verifying the clinic ID with search result");
		
		Web_GeneralFunctions.click(adPatSearch.getCancelBtnOnPatientDetails(Login_Doctor.driver, logger), "Click on cancel from Book_Appointment Page", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.scrollToElement(asPage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(adPatSearch.getAppointmentDropdown(Login_Doctor.driver, logger, patientName), "Clicking on dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		cancelAppointment();
		Web_GeneralFunctions.wait(3);
		
		//Govt ID
		
		Web_GeneralFunctions.scrollElementToCenter(asPage.getPlusIconforAvailableSlot(slotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(asPage.getPlusIconforAvailableSlot(slotId, Login_Doctor.driver, logger), "Clicking on Plus Icon", Login_Doctor.driver, logger);
		
		validInput="@"+SheetsAPI.getDataProperties(Web_Testbase.input+".GovtID");
		Web_GeneralFunctions.sendkeys(asPage.getSearchBoxinAvailableSlot(slotId, Login_Doctor.driver, logger), validInput, "Searching based on GovtID", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		patientName=adPatSearch.getPatientNameSearchResultSelect(Login_Doctor.driver, logger).get(0).getText();
		bookAppointment();
		
		Web_GeneralFunctions.scrollElementByJavaScriptExecutor(asPage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), "Scroll to the element",Login_Doctor.driver,logger);
		Web_GeneralFunctions.wait(5);
		Web_GeneralFunctions.click(adPatSearch.getAppointmentPhoto(Login_Doctor.driver, logger,patientName),"Click on Profile Pic", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);
		patientClinic=adPatSearch.getGovtIDFromPatientDetails(Login_Doctor.driver,logger).getAttribute("value");
		Assert.assertTrue(patientClinic.equals(validInput.substring(1)), "Verifying the clinic ID with search result");
		
		Web_GeneralFunctions.click(adPatSearch.getCancelBtnOnPatientDetails(Login_Doctor.driver, logger), "Click on cancel from Book_Appointment Page", Login_Doctor.driver, logger);	
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.scrollToElement(asPage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(adPatSearch.getAppointmentDropdown(Login_Doctor.driver, logger, patientName), "Clicking on dropdown", Login_Doctor.driver, logger);
		cancelAppointment();
		Web_GeneralFunctions.click(appoiPage.getAllSlotsPage(Login_Doctor.driver, logger), "Moving to Appointments Page", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);
		
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch77() throws Exception {
		logger=Reports.extent.createTest("Verify Inline Advance Search Book appointment and consultation button");
		
		//Moving to All slots Page
		Web_GeneralFunctions.click(appoiPage.getAllSlotsPage(Login_Doctor.driver, logger), "Moving to All slots page from Appointments Page", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);
		
		//Getting Available Slot Code
		int n = asPage.getAvailableSlot(Login_Doctor.driver, logger);
		String slotId = asPage.getSlotId(n, Login_Doctor.driver, logger);
				
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.scrollElementToCenter(asPage.getPlusIconforAvailableSlot(slotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(asPage.getPlusIconforAvailableSlot(slotId, Login_Doctor.driver, logger), "Clicking on Plus Icon", Login_Doctor.driver, logger);
		
		//UHID search
		String validInput="*"+SheetsAPI.getDataProperties(Web_Testbase.input+".UHID");
		Web_GeneralFunctions.sendkeys(asPage.getSearchBoxinAvailableSlot(slotId, Login_Doctor.driver, logger), validInput, "Searching based on UHID", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		if(specificRole.equals("NEWDOCNOCALCENTRE")||specificRole.equals("NewCalCentre"))
		{
		adPatSearch.getBookAppt_ConsulationBtn(Login_Doctor.driver, logger, BOOK_APPOINTMENT).isDisplayed();
		adPatSearch.getBookAppt_ConsulationBtn(Login_Doctor.driver, logger, "Consultation").isDisplayed();
		}
		else
			adPatSearch.getBookAppt_ConsulationBtn(Login_Doctor.driver, logger, BOOK_APPOINTMENT).isDisplayed();
		
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch78() throws Exception {
		logger=Reports.extent.createTest("Verify Inline Advance Search Option");

		Web_GeneralFunctions.click(asPage.getPatientRegisterBtn(Login_Doctor.driver, logger), "Clicking on Patient Register Button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);
	
	//Now Adding title
		Web_GeneralFunctions.click(parPage.getTitleDropDown(Login_Doctor.driver, logger), "Clicking to Save without any Mandatory Details", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(parPage.setTitlefromDropdown(Login_Doctor.driver, logger), "Clicking to Save ", Login_Doctor.driver, logger);
		
	
	//Now Adding Name too and Checking Alert Again
		Web_GeneralFunctions.sendkeys(parPage.getNameTextBox(Login_Doctor.driver, logger), RandomStringUtils.randomAlphabetic(10), "Clicking to Save without any Mandatory Details", Login_Doctor.driver, logger);
		String regname=parPage.getNameTextBox(Login_Doctor.driver, logger).getAttribute("value");
	
	//Now Adding Gender too and Checking Alert Again
		if(parPage.getGenderDropDown(Login_Doctor.driver, logger).isEnabled()) {
			Web_GeneralFunctions.click(parPage.getGenderDropDown(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(parPage.setGenderfromDropdown(Login_Doctor.driver, logger),"Clicking to Save", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(parPage.getSaveBtn(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
		}
		
	//Now Adding Age(Also Checking Age to DOB Conversion) and Checking
		Web_GeneralFunctions.sendkeys(parPage.getAgeinYears(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(2), "", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(parPage.getAgeinMonths(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(1), "", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(parPage.getAgeinDays(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(1), "", Login_Doctor.driver, logger);
		
	//Adding Mobile Number and Saving
		String mobileNo = SheetsAPI.getDataProperties(Web_Testbase.input+".InlineMobNo");
		Web_GeneralFunctions.sendkeys(parPage.setMobileNo(Login_Doctor.driver, logger), mobileNo, "", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(parPage.getSaveBtn(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
		
	
	//Saving with Existing Patient
		Web_GeneralFunctions.click(popupPage.getExistingPatientSelection(Login_Doctor.driver, logger), "Clicking to save as Existing Patient", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(popupPage.getConfirmBtn(Login_Doctor.driver, logger), "Clicking to Confirm", Login_Doctor.driver, logger);
		
		Web_GeneralFunctions.wait(5);
		
	//Searching based on Duplicate Patient Name
		Web_GeneralFunctions.click(AdvancePatientSearch.getAdvanceSearchBtn(Login_Doctor.driver, logger), "Clicking to go back to advance search page", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(adPatSearch.getMobileField(Login_Doctor.driver, logger), mobileNo,"Entering Alphabets ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		
		List<WebElement> resultNames = adPatSearch.getPatientNameSearchResultSelect(Login_Doctor.driver, logger);
		for (WebElement name : resultNames) {
			Assert.assertFalse(name.getText().toLowerCase().contains(regname),"List of Element Searched with Valid Name");
		}
		adPatSearch.getSearchNameText(Login_Doctor.driver, logger).clear();

	}
	public synchronized void bookAppointment() throws Exception
	{
		 Web_GeneralFunctions.click(adPatSearch.getBookAppt_ConsulationBtn(Login_Doctor.driver, logger, BOOK_APPOINTMENT), "Booking appointment",Login_Doctor.driver, logger); Web_GeneralFunctions.wait(3);
		 Web_GeneralFunctions.sendkeys(adPatSearch.getReasonForDocVisit(Login_Doctor.driver, logger), RandomStringUtils.randomAlphabetic(10),"Reason to visit", Login_Doctor.driver, logger);
		 Web_GeneralFunctions.click(adPatSearch.getSaveAndConfirm(Login_Doctor.driver,logger), "Save And Confirm", Login_Doctor.driver, logger);
		 Web_GeneralFunctions.wait(8);
		
	}
	
	public synchronized void cancelAppointment() throws Exception
	{
		int num = asPage.getCancelfromDropdownNum(Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(asPage.getCancelfromDropdown(num, Login_Doctor.driver, logger),"Clicking on cancel in Dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(asPage.getReasondropdownfromCancelAlert(Login_Doctor.driver, logger),"Clicking to get reasons in Cancel Alert", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(asPage.setReasoninCancelAlert(Login_Doctor.driver, logger),"Clicking on reasons in Cancel Alert", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(asPage.getSubmitBtnfromCancelAlert(Login_Doctor.driver, logger),"Clicking on Submit in Alert", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);
	}
	
}
