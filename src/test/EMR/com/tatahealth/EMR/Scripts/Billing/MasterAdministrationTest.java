package com.tatahealth.EMR.Scripts.Billing;

import org.openqa.selenium.JavascriptExecutor;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Billing.Billing;
import com.tatahealth.EMR.pages.Billing.MasterAdministration;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class MasterAdministrationTest {
	public static ExtentTest logger;
	Billing bill = new Billing();
	MasterAdministration admin=new MasterAdministration();
	
	public void masterAdministrationBillType(String billType) throws Exception
	{
		logger = Reports.extent.createTest("EMR_Master Adminstration Bill Type");
		JavascriptExecutor je = (JavascriptExecutor) Login_Doctor.driver;
		Web_GeneralFunctions.wait(2);
		bill.getEmrTopMenuElement(Login_Doctor.driver, logger,"Master Administration").click();
		je.executeScript("arguments[0].click();",admin.getEmrMasterAdminTab(Login_Doctor.driver, logger,"Configuration Master"));	
		admin.getEmrMasterAdminConfigMasterSearchTextBox(Login_Doctor.driver, logger).sendKeys("Billing Type");
		admin.getEmrMasterAdminConfigMasterSearchEdit(Login_Doctor.driver, logger).click();
		admin.getEmrMasterAdminConfigMasterBillTypeDropdown(Login_Doctor.driver, logger, billType);
		admin.getEmrMasterAdminConfigMasterSaveButton(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(4);
	}

}
