package com.tatahealth.EMR.Scripts.ReportsView;
import java.util.Random;
import java.util.regex.Pattern;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.pages.ReportsView.ReportsView;

public class ReportsView_upload {
	ReportsView report = new ReportsView();
	public static ExtentTest logger;
	public static String UHID;
	Random random = new Random();
	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		Reports.reports();
		Login_Doctor.executionName = "reportsViewUpload";
		Login_Doctor.LoginTestwithDiffrentUser("Kasu");
	}
	@AfterClass(alwaysRun = true)
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		Reports.extent.flush();
	}
	public synchronized void reportsViewUploadLunch() throws Exception {
		logger = Reports.extent.createTest("EMR_Reports View/Upload Page");
		report.getEmrTopMenu(Login_Doctor.driver, logger).click();
		report.getEmrTopMenuElement(Login_Doctor.driver, logger, "Reports").click();
		report.getEmrTopMenu(Login_Doctor.driver, logger).click();
	}
	@Test(priority = 1, groups = { "Regression", "ReportView" })
	public void clickOnReportsViewUploadMenu() throws Exception {
		logger = Reports.extent.createTest("Check when user clicks on Reports view/upload module");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String searchPatient = report.getEmrSeachPatientBtn(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("SEARCH PATIENT".equals(searchPatient.trim()), "Search Patient button not found");
	}
	@Test(priority = 2, groups = { "Regression", "ReportView" })
	public void fieldsOnSearchPatientModule() throws Exception {
		logger = Reports.extent.createTest("To check what all the field available for search patient report");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		Web_GeneralFunctions.isVisible(report.getUHIDField(Login_Doctor.driver, logger),Login_Doctor.driver);
		Web_GeneralFunctions.isVisible(report.getNameField(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.isVisible(report.getDOBField(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.isVisible(report.getGenderField(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.isVisible(report.getMobileField(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.isVisible(report.getFromDateField(Login_Doctor.driver, logger), Login_Doctor.driver);
		Assert.assertTrue(report.getUHIDField(Login_Doctor.driver, logger).isDisplayed(), "UHID Field displayed");
		Assert.assertTrue(report.getNameField(Login_Doctor.driver, logger).isDisplayed(), "Name Field displayed");
		Assert.assertTrue(report.getDOBField(Login_Doctor.driver, logger).isDisplayed(), "DOB Field displayed");
		Assert.assertTrue(report.getGenderField(Login_Doctor.driver, logger).isDisplayed(), "Gender Field displayed");
		Assert.assertTrue(report.getMobileField(Login_Doctor.driver, logger).isDisplayed(), "Mobile Field displayed");
		Assert.assertTrue(report.getFromDateField(Login_Doctor.driver, logger).isDisplayed(),
				"From Date Field displayed");
		Assert.assertTrue(report.getToDateField(Login_Doctor.driver, logger).isDisplayed(), "To Date Field displayed");
	}
	@Test(priority = 3, groups = { "Regression", "ReportView" })
	public void validateTextEntryOnUHIDField() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to enter alphabets in UHID field");
		Web_GeneralFunctions.wait(1);
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String randonText = Web_GeneralFunctions.getRandomNumber1(10);
		Web_GeneralFunctions.isVisible(report.getUHIDField(Login_Doctor.driver, logger), Login_Doctor.driver);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys(randonText);
		if (!Pattern.matches("[a-zA-Z]", report.getUHIDField(Login_Doctor.driver, logger).getText())) {
			Assert.assertTrue(true, "TextField doesn't accept character alphabet");
		} else {
			Assert.assertTrue(false, "TextField accepts character alphabet");
		}
	}
	@Test(priority = 4, groups = { "Regression", "ReportView" })
	public void validateNumbertEntryOnUHIDField() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to enter numeric in UHID field ");
		Web_GeneralFunctions.wait(1);
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String uhid = RandomStringUtils.randomNumeric(4);
		Web_GeneralFunctions.isVisible(report.getUHIDField(Login_Doctor.driver, logger), Login_Doctor.driver);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys(uhid);
		Web_GeneralFunctions.isVisible(report.getEmrSeachPatientBtn(Login_Doctor.driver, logger), Login_Doctor.driver);
		report.getEmrSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		if (report.getUHIDField(Login_Doctor.driver, logger).getText().isEmpty()) {
			Web_GeneralFunctions.isVisible(report.searchResult(Login_Doctor.driver, logger), Login_Doctor.driver);
			Assert.assertEquals(report.searchResult(Login_Doctor.driver, logger).getText(), "No Result Found");
		} else {
			Assert.assertTrue(false, "User cannot enter number on UHID field");
		}
	}
	@Test(priority = 5, groups = { "Regression", "ReportView" })
	public void validateSpecialChartEntryOnUHIDField() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to enter special char in UHID field");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String special = "^((?=[A-Za-z0-9@])(?![_\\\\\\\\-]).)*$";
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys(special);
		if (!Pattern.matches("[^((?=[A-Za-z0-9@])(?![_\\\\\\\\-]).)*$]",
				report.getUHIDField(Login_Doctor.driver, logger).getText())) {
			Assert.assertTrue(true, "Do not allow special characters");
		} else {
			Assert.assertTrue(false, "Allow special characters");
		}
	}
	@Test(priority = 6, groups = { "Regression", "ReportView" })
	public void seachPatientWithUHID() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to search patient  only with UHID");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String UHID = "8117";
		Web_GeneralFunctions.isVisible(report.getUHIDField(Login_Doctor.driver, logger), Login_Doctor.driver);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys(UHID);
		Web_GeneralFunctions.isVisible(report.getSeachPatientBtn(Login_Doctor.driver, logger), Login_Doctor.driver);
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		try {
			Web_GeneralFunctions.isVisible(report.getMobileNumber(Login_Doctor.driver, logger), Login_Doctor.driver);
			Assert.assertTrue(report.getMobileNumber(Login_Doctor.driver, logger).isDisplayed());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test(priority = 7, groups = { "Regression", "ReportView" })
	public void invalidUHIDSearch() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to enter numeric in UHID field ");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String UHID = RandomStringUtils.randomNumeric(5);

		report.getUHIDField(Login_Doctor.driver, logger).sendKeys(UHID);
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();

		String actualText = report.searchResult(Login_Doctor.driver, logger).getText();

		Assert.assertEquals(actualText, "No Result Found");

	}

	@Test(priority = 8, groups = { "Regression", "ReportView" })
	public void numberOfDigitsInUHIDField() throws Exception {
		logger = Reports.extent.createTest("To check how many digits user able to enter in UHID field");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String UHID = RandomStringUtils.randomNumeric(500);	
		Web_GeneralFunctions.isVisible(report.getUHIDField(Login_Doctor.driver, logger), Login_Doctor.driver);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys(UHID);
		Web_GeneralFunctions.wait(5);
		if (report.getUHIDField(Login_Doctor.driver, logger).getAttribute("value").length()==500) {
			Assert.assertTrue(true, "There is no limit in length");
		} else {
			Assert.assertTrue(false, "User cannot enter 500 digits on UHID field");
		}
		Web_GeneralFunctions.isVisible(report.getUHIDField(Login_Doctor.driver, logger), Login_Doctor.driver);
		report.getUHIDField(Login_Doctor.driver, logger).clear();
		Web_GeneralFunctions.wait(2);
	}
	@Test(priority = 9, groups = { "Regression", "ReportView" })
	public void alphabetsOnNameField() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to enter alphabets in name field ");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String name = RandomStringUtils.randomAlphabetic(8).toUpperCase();
		report.getNameField(Login_Doctor.driver, logger).sendKeys(name);
		Web_GeneralFunctions.wait(2);
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Assert.assertEquals(report.searchResult(Login_Doctor.driver, logger).getText(), "No Result Found");
	}
	@Test(priority = 10, groups = { "Regression", "ReportView" })
	public void numberOnNameField() throws Exception {
		logger = Reports.extent.createTest("To check whether user is not able to enter numeric in name field");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String number = String.format("%04d", random.nextInt(10000));
		Web_GeneralFunctions.isVisible(report.getNameField(Login_Doctor.driver, logger), Login_Doctor.driver);
		report.getNameField(Login_Doctor.driver, logger).sendKeys(number);
		if (report.getNameField(Login_Doctor.driver, logger).getText().isEmpty()) {
			Assert.assertTrue(true, "User cannot enter number on Name field");
		} else {
			Assert.assertTrue(false, "User can enter number on Name field");
		}
	}
}
