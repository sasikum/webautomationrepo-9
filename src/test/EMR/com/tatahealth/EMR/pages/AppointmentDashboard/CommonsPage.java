package com.tatahealth.EMR.pages.AppointmentDashboard;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;



public class CommonsPage {
	
	public static String getPageType(WebDriver driver, ExtentTest logger) {
		String currentPage = Web_GeneralFunctions.findElementbySelector("#appointmentToggle", driver, logger).getText();
		if(currentPage.equalsIgnoreCase("All Slots")) {
			System.out.println("We are in Appointments Page");
		}else if(currentPage.equalsIgnoreCase("Appointments")) {
			System.out.println("We are in All Slots Page");
		}else {
			System.out.println("we are in some other page. Please Check");
		}
		return currentPage;
		
	}

	public WebElement getlogOutImg(WebDriver driver,ExtentTest logger) 
	{
		WebElement logOut = Web_GeneralFunctions.findElementbyXPath("//div[@class='pull-left topiconpadtop']//a//img", driver, logger);
		return logOut;	
	}
	public WebElement getlogOutDropdown(WebDriver driver,ExtentTest logger) 
	{
		WebElement logOut = Web_GeneralFunctions.findElementbyXPath("//img[@id='logout-dropdown']", driver, logger);
		return logOut;	
	}
	public WebElement getChooseClinic(WebDriver driver,ExtentTest logger) 
	{
		WebElement chooseClinic = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Choose Clinic')]", driver, logger);
		return chooseClinic;	
	}
	
	public WebElement getLogOut(WebDriver driver,ExtentTest logger) 
	{
		WebElement LogOut = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Logout')]", driver, logger);
		return LogOut;	
	}
	
	public List<WebElement> getNumberOfClinic(WebDriver driver,ExtentTest logger) 
   	{
   		List<WebElement> userProfileLink = Web_GeneralFunctions.findElementsbyXpath("//div[@class='globalclinicbox']", driver, logger);
    		return userProfileLink;	
   	}
	public WebElement getFristClinic(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement servicesOfferedTbx = Web_GeneralFunctions.findElementbyXPath("(//div[@class='globalclinicbox'])[1]/img",driver,logger);
   			return servicesOfferedTbx;	
   	}
	
	public WebElement getSecondClinic(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement servicesOfferedTbx = Web_GeneralFunctions.findElementbyXPath("(//div[@class='globalclinicbox'])[2]/.",driver,logger);
   			return servicesOfferedTbx;	
   	}
	//*************************** Elements for 1st login after signup *****************************************************//
	
	public WebElement getIAgreeCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement iAgreeCbx = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'I Agree')]",driver,logger);
   			return iAgreeCbx;	
   	}
	
	public WebElement getProceedBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement proceedBtn = Web_GeneralFunctions.findElementbyXPath("//button[@id='proceed']",driver,logger);
   			return proceedBtn;	
   	}
	
	public WebElement getCancelBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement cancelBtn = Web_GeneralFunctions.findElementbyXPath("//button[@id='cancelTerms']",driver,logger);
   			return cancelBtn;	
   	}
	
}