package com.tatahealth.EMR.pages.Consultation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class GlobalPrintPage {

	public WebElement getGlobalPrintModule(WebDriver driver,ExtentTest logger) throws Exception{
		
		//String xpath = "//ul[@id='fixedmenu']/li[6]";
		String xpath = "//ul[@id='fixedmenu']/li[@data-input='Print']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getPrintAllButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//a[@id='TransferPrescription']", driver, logger);
		return element;
	}
	
	public WebElement getPrintButton(WebDriver driver,ExtentTest logger) throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//a[@class='btnmarginleftPrint']", driver, logger);
		return element;
		
	}
	
	public WebElement getShareToFollowUpButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//button[@id='softCheckout']", driver, logger);
		return element;
		
	}
	
	public WebElement getCheckOutButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//button[@id='checkoutButton']", driver, logger);
		return element;
		
	}
	
	public boolean getEmptyDiagnosisAlertPopUp(WebDriver driver,ExtentTest logger)throws Exception{
		
		boolean status = false;
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//div[@class='sweet-alert showSweetAlert visible']", driver, logger);
		
		if(element.isDisplayed()) {
			status = true;
		}else{
			status = false;
		}
		return status;
	}
	
	public WebElement getEmptyDiagnosisConfirm(WebDriver driver,ExtentTest logger)throws Exception{
		
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//button[@class='confirm']", driver, logger);
		return element;
		
	}
	
	
	public WebElement consultationSectionsGA(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='GenericAdvicePrintSelection']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
   public WebElement consultationSectionsCaseSheet(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='ExaminationPrintSelection']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
   public WebElement consultationSectionsReferral(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(text(),'Referral')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
   public WebElement consultationSectionsMedicalHistory(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(text(),'Medical History')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
   
   public WebElement consultationSectionsVitals(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(text(),'Vitals')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
   
   public WebElement consultationSectionsDiagnosis(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(text(),'Diagnosis')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
   
   
   public WebElement consultationSectionsFollowUp(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(text(),'FollowUp')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
   
   
   public WebElement consultationSectionsLabTests(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(text(),'Lab Tests')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
   
   public WebElement consultationSectionsSymptoms(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='SymptomsPrintSelection']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
   public WebElement consultationSectionsPrescription(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(text(),'Prescription')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	public WebElement setAsDefaultButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[contains(text(),'Set as default')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getSaveAsMedicalKit(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[contains(text(),'Save as Medical Kit')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
		
	}
	
	public WebElement saveMedicalKit(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@id='saveMedicalKit']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getMedicalKitName(WebDriver driver,ExtentTest logger)throws Exception{
		
		
		String xpath = "//input[@id='userDefinedKitName']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getMedicalKitClosePopup(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='medicalKitCreateNewModal']//span[contains(text(),'×')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement moveToSymptomModule(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//ul[@id='fixedmenu']//li[@data-input='consultvitals']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getMedicalKitSearch(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='medicalKitSearch']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
    public WebElement getAppyMedicalKitButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//*[@id='medicalKitApplyButton']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
 	public WebElement getMedicalKitFromDropDown(String value,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='medicalKitSearch']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		

		//Web_GeneralFunctions.click(element, "click medical kit search", driver, logger);
		//Web_GeneralFunctions.sendkeys(element, value, "sending medical kit search", driver, logger);

		Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		
		generalFunctions.click(element, "click medical kit search", driver, logger);
		generalFunctions.sendkeys(element, value, "sending medical kit search", driver, logger);

		Thread.sleep(1000);
		
		xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li[1]/a";
		
		//System.out.println("xpath : "+xpath);
		
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
				
		return element;
		
	}
	
	public WebElement getViewSummaryButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='printSummarybtn']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getViewSummaryCloseButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='checkoutClose']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
}
